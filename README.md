# OneShot SSH

OneShot SSH is a script that allow fast and secured ssh connection without having to pass through keeper commander or going to keeper web browser, and without having any ssh keys on computers.

# Usage

## Pre-requisites

Have expect installed on your machine  

- Linux and Windows WSL (Ubuntu example) :
```
apt-get install expect
```

- Mac :
```
brew install expect
```

## Import requirements

Find import requirements in the setup.py file.  

To install it : 
```
python3 setup.py install
```

## Setup

To make the script running properly, you have to change config.cfg.template and ksm-config.json.template as config.cfg and ksm-config.json and fill it with the rights fields, corresponding to your project. ksm-config.json must be generated directly by Keeper when adding your machine in the corresponding Keeper Application.

You also must have the server credentials shared with you from keeper application.

## Usage

In a shell, just go to this project and type the following bash command for help
```
python3 src/oneshot.py -h
```

Commands with OneShot are just like basic SSH and SCP but with oneshot as prefix :

### OneShot SSH

To SSH with OneShot just type the following bash command
```
python3 src/oneshot.py ssh user@host
```
For example, you can type the following bash command to connect to server 10.0.0.1 as sysadmins
```
python3 src/oneshot.py ssh sysadmins@10.0.0.1
```

### OneShot SCP

To SCP with OneShot just type the following bash command
```
python3 src/oneshot.py scp sourceFile destinationFile
```
For example, you can type the following bash command to send test.py to server 10.0.0.1 as root
```
python3 src/oneshot.py scp /home/user/test.py root@10.0.0.1:/home/user
```
