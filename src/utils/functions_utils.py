import subprocess
from sys import stderr
from .keeper_utils import get_all_secrets


def find_right_arg(str):
    for char in str:
        if char == "@":
            return True
    return False


def get_arg(args):
    for arg in args:
        if find_right_arg(arg) is True:
            res = arg.split("@")
            user = res[0]
            res2 = res[1].split(":")
            host = res2[0]
            return user, host
    raise Exception


def oneshot_command(
    keeper_file_key_value_storage, action, user, host, source=None, destination=None
):
    bool_connection_found = False
    bool_ip_found = False

    secrets = get_all_secrets(keeper_file_key_value_storage)
    for secret in secrets:
        keeper_host = secret.dict["fields"][0]["value"][0]["hostName"]
        keeper_user = secret.dict["fields"][1]["value"][0]
        if host == keeper_host:
            bool_ip_found = True
            if keeper_user == user:
                this_keeper_password = secret.dict["fields"][2]["value"][0]
                this_keeper_host = keeper_host
                this_keeper_user = keeper_user
                bool_connection_found = True

    if bool_connection_found is True:
        if action == "ssh":
            subprocess.run(
                [
                    "expect",
                    "src/expect/oneshot_ssh.expect",
                    this_keeper_user,
                    this_keeper_host,
                    this_keeper_password,
                ]
            )
        elif action == "scp":
            subprocess.run(
                [
                    "expect",
                    "src/expect/oneshot_scp.expect",
                    source,
                    destination,
                    this_keeper_password,
                ]
            )

    else:
        stderr.write("Connection failed\n")
        if bool_ip_found is False:
            stderr.write("IP adress not found\n")
        else:
            stderr.write("Login not found for this IP adress\n")
