from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser(
        description="ssh or scp in one line",
        epilog="hope it helped you",
    )

    parser.add_argument(
        "action",
        choices=["ssh", "scp"],
        type=str,
        help="ssh or scp (default is ssh)",
        nargs=1,
    )
    parser.add_argument(
        "args",
        type=str,
        help="arg(s) for ssh or scp",
        nargs="+",
    )

    return parser.parse_args()


"""def parse_args():
    parser = ArgumentParser(
        description="ssh or scp in one line",
        epilog="hope it helped you",
    )

    parser.add_argument(
        "-a",
        "--action",
        type=str,
        required=False,
        help="ssh or scp (default is ssh)",
        nargs=1,
        default=["ssh"],
    )
    parser.add_argument(
        "-u",
        "--user",
        type=str,
        required=True,
        help="username",
        nargs=1,
    )
    parser.add_argument(
        "-ip",
        "--host",
        type=str,
        required=True,
        help="ip adress to ssh to",
        nargs=1,
    )
    parser.add_argument(
        "-f",
        "--files",
        type=str,
        required=False,
        help="if scp, files locations in the format: source destination",
        nargs=2,
    )

    return parser.parse_args()"""
