from keeper_secrets_manager_core import SecretsManager
from keeper_secrets_manager_core.storage import FileKeyValueStorage


def get_all_secrets(keeper_file_key_value_storage):
    secrets_manager = SecretsManager(
        config=FileKeyValueStorage(keeper_file_key_value_storage)
    )
    return secrets_manager.get_secrets()
