from sys import stderr
from configparser import ConfigParser
from utils.functions_utils import oneshot_command, get_arg
from utils.parser_utils import parse_args

config = ConfigParser()
config.read("config/config.cfg")

KEEPER_FILE_KEY_VALUE_STORAGE = config.get(
    "keeper_config_file", "keeper_file_key_value_storage"
)

args = parse_args()

user, host = get_arg(args.args)

if args.action[0] == "ssh":
    oneshot_command(KEEPER_FILE_KEY_VALUE_STORAGE, args.action[0], user, host)
elif args.action[0] == "scp":
    oneshot_command(
        KEEPER_FILE_KEY_VALUE_STORAGE,
        args.action[0],
        user,
        host,
        args.args[0],
        args.args[1],
    )
else:
    stderr.write("Command not found (must be ssh or scp)\n")
