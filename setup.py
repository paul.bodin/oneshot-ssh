"""import every important module"""
from setuptools import setup, find_packages

setup(
    name="oneshot-ssh",
    version="1.0",
    description="Allow fast and secured SSH",
    author=["Paul Bodin"],
    authors_emails=[
        "paulbdin@gmail.com",
    ],
    packages=find_packages(),
    url="https://gitlab.com/paul.bodin/oneshot-ssh",
    install_requires=[
        "keeper_secrets_manager_core",
    ],
)
